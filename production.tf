provider "aws" {
  region     = var.region
}
resource "random_id" "random_id_prefix" {
  byte_length = 2
}

locals {
  # production_availability_zones = ["${var.region}a", "${var.region}b", "${var.region}c"]
  cwd           = reverse(split("/", path.cwd))
  instance_type = local.cwd[1]
  location      = local.cwd[2]
  environment   = local.cwd[3]
  vpc_cidr      = "10.0.0.0/16"
}

module "Networking" {
  source               = "./modules/Networking"
  vpc_cidr             = local.vpc_cidr
  access_ip = var.access_ip
  public_sn_count      = 4
  private_sn_count     = 2
  db_subnet_group      = true
  availabilityzone     = ["us-east-1a" , "us-east-1b"]
  azs                  = 2
}

module "S3" {
  source = "./modules/S3"
  environment = var.environment
}

module "Database" {
  source               = "./modules/Database"
  db_storage           = 8
  db_engine_version    = "8.0"
  db_instance_class    = "db.t2.micro"
  db_name              = var.db_name
  dbuser               = var.dbuser
  dbpassword           = var.dbpassword
  db_identifier        = "pristine-poc-db"
  skip_db_snapshot     = true
  rds_sg               = module.Networking.rds_sg
  db_subnet_group_name = module.Networking.db_subnet_group_name[0]
}

module "compute" {
  source = "./modules/compute"
  frontend_app_sg = module.Networking.frontend_app_sg
  backend_app_sg = module.Networking.backend_app_sg
  bastion_sg =  module.Networking.bastion_sg
  public_subnets = module.Networking.public_subnets
  private_subnets = module.Networking.private_subnets
  bastion_instance_count = 1
  instance_type = local.instance_type
  key_name = "Pristine_POC"
  lb_tg = module.loadbalancing.lb_tg
  lb_tg_name = module.loadbalancing.lb_tg_name

  
}

module "loadbalancing" {
  source = "./modules/loadbalancing"
  lb_sg = module.Networking.lb_sg
  public_subnets = module.Networking.public_subnets
  tg_port = 80
  tg_protocol = "HTTP"
  vpc_id = module.Networking.vpc_id
  app_asg = module.compute.app_asg
  listener_port = 80
  listener_protocol = "HTTP"
  azs = 2
  
}

