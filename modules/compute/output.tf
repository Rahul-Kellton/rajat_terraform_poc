output "app_asg" {
  value = aws_autoscaling_group.pristine_poc_app
}

output "app_backend_asg" {
  value = aws_autoscaling_group.pristine_poc_backend
}