resource "aws_s3_bucket" "s3" {
  
tags = {
    Name        = "${var.environment}-s3"
    Environment = var.environment
  }
}


resource "aws_s3_bucket_acl" "acl" {
  bucket = aws_s3_bucket.s3.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.s3.id
  versioning_configuration {
    status = "Enabled"
  }
}